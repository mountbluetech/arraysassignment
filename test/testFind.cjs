let items = require('../items.cjs');
let find = require('../find.cjs');

console.log(find(items, function cb(element, elements, ind) {
    if(ind == elements.length) return false;
    else if(elements[ind] === element) return true;
    else return cb(element, elements, ind+1);
}));