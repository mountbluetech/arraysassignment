function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test

    let result = [];
    let element = 20;
    for(let ele in elements){
        if(cb(elements[ele], ele, elements) === true) result.push(elements[ele]);
    }
    return result;
}

module.exports = filter;