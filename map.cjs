function map(elements, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
    let mapped = [];
    for(let ind=0; ind < elements.length;ind++){
        mapped.push(cb(elements[ind], ind, elements));
       // console.log(elements[ind]);
    }
    return mapped;
}

module.exports = map;