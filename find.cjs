let items = require("./items.cjs");

function find(elements, cb) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
       let ele = 1280;
       if(cb(ele,elements,0)) return ele;
       else return undefined; 
}

module.exports = find;